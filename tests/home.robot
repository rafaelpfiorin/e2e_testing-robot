*** Settings ***

Library     AppiumLibrary

*** Variables ***
${NOME}     Rafael P. Fiorin
# lista
@{CARROS}   Civic   Lancer  Chevette    Brasilia
# dicionario
&{CARRO}    nome=Lancer     modelo=Evolution    ano=2020


*** Test Cases ***
Deve abrir a tela principal
    Open Application    http://localhost:4723/wd/hub
    ...                 automationName=UiAutomator2
    ...                 platformName=Android
    ...                 deviceName=Emulator
    ...                 app=${EXECDIR}/app/twp.apk
    # faz assercao no texto da tela em ate 5s
    Wait Until Page Contains    Training Wheels Protocol    5
    Wait Until Page Contains    Mobile Version              5
    Close Application
*** Settings ***
Resource    ../resources/base.robot

Test Setup      Open Session
Test Teardown   Close Session

*** Variables ***
${SPINNER}=     id=io.qaninja.android.twp:id/spinnerJob
${LIST_OPTIONS}=    class=android.widget.ListView
${BTN_REMOVE}=  id=io.qaninja.android.twp:id/btnRemove

*** Keywords ***
Choice Job
    [Arguments]     ${target}

    Click Element   ${SPINNER}
    Wait Until Element Is Visible   ${LIST_OPTIONS}

    Click Text  ${target}

*** Test Cases ***
Deve remover o capitão
    Go To Long Avenger List

    Swipe By Percent    88.88   18.22   47.22   18.22
    Wait Until Element Is Visible   ${BTN_REMOVE}

    Click Element                   ${BTN_REMOVE}
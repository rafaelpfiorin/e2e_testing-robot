*** Settings ***
Resource    ../resources/base.robot

# hook (executa antes de cada teste)
Test Setup  Open Session
# hook (executa depois de cada teste)
Test Teardown   Close Session

*** Variables ***
${TOOLBAR_TITLE}    id=io.qaninja.android.twp:id/toolbarTitle

*** Test Cases ***
Deve acessar a tela Dialogs
    Open Nav
     
    # estes comandos sao todos keywords
    Click Text                      DIALOGS

    Wait Until Element is Visible   ${TOOLBAR_TITLE}   
    Element Text Should Be          ${TOOLBAR_TITLE}   DIALOGS

Deve acessar a tela Forms   
    Open Nav
 
    # estes comandos sao todos keywords
    Click Text                      FORMS

    Wait Until Element is Visible   ${TOOLBAR_TITLE}   
    Element Text Should Be          ${TOOLBAR_TITLE}   FORMS

Deve acessar a tela de vingadores
    Open Nav

    # estes comandos sao todos keywords
    Click Text                      AVENGERS

    Wait Until Element is Visible   ${TOOLBAR_TITLE}   
    Element Text Should Be          ${TOOLBAR_TITLE}   AVENGERS
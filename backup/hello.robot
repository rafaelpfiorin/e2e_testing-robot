***Settings***
# tabulacao = TAB 2x
Library     hello.py

***Test Cases***
Deve retornar mensagem de boas vindas
    # entende que esta invocando o metodo de hello.py
    ${resultado}=   Hello Robot     Rafael
    #Log To Console  ${resultado}
    Should Be Equal     ${resultado}    Olá, Rafael!